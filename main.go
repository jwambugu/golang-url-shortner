package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"gitlab.com/jwambugu/hexagonal-ms/api"
	"gitlab.com/jwambugu/hexagonal-ms/pkg/shortner"
	"gitlab.com/jwambugu/hexagonal-ms/repository/shortner/mongodb"
	"gitlab.com/jwambugu/hexagonal-ms/repository/shortner/redis"
)

func httpPort() string {
	return fmt.Sprintf(":%d", 8000)
}

func chooseRepository(db string) shortner.RedirectRepository {
	switch db {
	case "redis":
		repo, err := redis.NewRedisRepository("redis://localhost:6379")

		if err != nil {
			log.Fatal(err)
		}

		return repo
	case "mongo":
		repo, err := mongodb.NewMongoRepository("", "", 30)

		if err != nil {
			log.Fatal(err)
		}

		return repo
	}

	return nil
}

func main() {
	repository := chooseRepository("redis")

	service := shortner.NewRedirectService(repository)
	handler := api.NewHandler(service)

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/{code}", handler.Get)
	r.Post("/", handler.Post)

	errs := make(chan error, 2)

	go func() {
		fmt.Printf("Listening on port %s \n", httpPort())

		errs <- http.ListenAndServe(httpPort(), r)
	}()

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT)

		errs <- fmt.Errorf("%s", <-c)
	}()

	fmt.Printf("Terminated: %s \n", <-errs)
}
