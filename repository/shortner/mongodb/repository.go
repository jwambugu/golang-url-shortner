package mongodb

import (
	"context"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"time"

	"gitlab.com/jwambugu/hexagonal-ms/pkg/shortner"
)

type mongoRepository struct {
	client   *mongo.Client
	database string
	timeout  time.Duration
}

func (m *mongoRepository) Find(code string) (*shortner.Redirect, error) {
	ctx, cancel := context.WithTimeout(context.Background(), m.timeout)

	defer cancel()

	redirect := &shortner.Redirect{}

	collection := m.client.Database(m.database).Collection("redirects")

	filter := bson.M{
		"code": code,
	}

	err := collection.FindOne(ctx, filter).Decode(&redirect)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.Wrap(shortner.ErrRedirectNotFound, "repository.shortner.Redirect.Find")
		}

		return nil, errors.Wrap(err, "repository.shortner.Redirect.Find")
	}

	return redirect, nil
}

func (m *mongoRepository) Store(redirect *shortner.Redirect) error {
	ctx, cancel := context.WithTimeout(context.Background(), m.timeout)

	defer cancel()

	collection := m.client.Database(m.database).Collection("redirects")

	_, err := collection.InsertOne(ctx, bson.M{
		"code":       redirect.Code,
		"url":        redirect.URL,
		"created_at": redirect.CreatedAt,
	})

	if err != nil {
		return errors.Wrap(err, "repository.shortner.Redirect.Store")
	}

	return nil
}

func newMongoClient(url string, timeout int) (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)

	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(url))

	if err != nil {
		return nil, err
	}

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, err
	}

	return client, nil
}

func NewMongoRepository(url, db string, timeout int) (shortner.RedirectRepository, error) {
	repo := &mongoRepository{
		database: db,
		timeout:  time.Duration(timeout) * time.Second,
	}

	client, err := newMongoClient(url, timeout)

	if err != nil {
		return nil, errors.Wrap(err, "repository.shortner.NewMongoRepository")
	}

	repo.client = client
	return repo, nil
}
